import * as d3 from 'd3';

export function drawHeaderDesktopChart() {
    let svg = d3.select('#bubble-chart');

    let prueba = [];
    for(let i = 0; i < 172; i++){
        prueba[i] = {'name': '1'};
    }
    
    // Initialize the circle: all located at the center of the svg area
    let node = svg
        .selectAll("circle")
        .data(prueba)
        .enter()
        .append("circle")
        .attr("r", 20)
        .attr("cx", document.getElementById('bubble-chart').clientWidth / 2)
        .attr("cy", document.getElementById('bubble-chart').clientHeight / 2)
        .style("fill", "none")
        .attr("stroke", "#fff")
        .style("stroke-width", 1.5);

    let simulation = d3.forceSimulation()
        .force('charge', d3.forceManyBody().strength(-36))
        .force('collision', d3.forceCollide().radius(22))
        .alphaDecay(0)
        .force("x", d3.forceX().strength(0.05))
        .force("y", d3.forceY().strength(0.05))
        .force("center", d3.forceCenter()
            .x(415)
            .y(415)
        );

    simulation
        .nodes(prueba)
        .on("tick", function(d){
            node
            .attr("cx", function(d){ return d.x; })
            .attr("cy", function(d){ return d.y; })
        });
}

export function drawHeaderMobileChart() {
    let svgNode = d3.select('.header__chart--mobile').node();
    let w = svgNode.clientWidth;
    let h = svgNode.clientHeight;

    let svg = d3.select('.header__chart--mobile').append('svg').attr('width', w).attr('height', h);
    

    let circle = svg.selectAll("circle")
        .data(d3.range(172).map(function() {
            return {
                x: w * Math.random(),
                y: h * Math.random(),
                dx: Math.random() - 0.5,
                dy: Math.random() - 0.5
            };
        }))
        .enter()
        .append("circle")
        .attr("r", 7.5)
        .style('fill', 'none')
        .style('stroke', '#fff')
        .style('opacity', 0.2)
        .style('stroke-width', '1.5px');

    let start = Date.now(), frames = 0;
    
    d3.timer(function() {    
      // Update the FPS meter.
      let now = Date.now(), duration = now - start;
      if (duration >= 1000) frames = 0, start = now;
    
      // Update the circle positions.
      circle
          .attr("cx", function(d) { d.x += d.dx; if (d.x > w) d.x -= w; else if (d.x < 0) d.x += w; return d.x; })
          .attr("cy", function(d) { d.y += d.dy; if (d.y > h) d.y -= h; else if (d.y < 0) d.y += h; return d.y; });    
    });
}