import { scrollActiv, setRRSSLinks } from '../../common_projects_utilities/js/pure-branded-helpers';
import { isMobile, isElementInViewport } from '../../common_projects_utilities/js/dom-helpers';
import { drawHeaderDesktopChart, drawHeaderMobileChart } from './headerChart';
//Necesario para importar los estilos de forma automática en la etiqueta 'style' del html final
import '../css/main.scss';

let viewportHeight = window.innerHeight;
let viewportWidth = window.innerWidth;
let footerAnchor = document.getElementById("footer-anchor");
let sharing = document.getElementById("sharing");
let sectionHeaders = document.getElementsByClassName('block__outer');
let menuTitles = [
	"01 · Gobernanza, preparación y la toma de decisiones",
	"02 · Fuentes de información",
	"03 · Acciones tomadas y alternativas disponibles",
	"04 · Aspectos sociales",
	"05 · Comunicación de riesgo y recomendaciones a la población"
];
let currentScroll = 0;

setRRSSLinks();

if(!isMobile()){
	drawHeaderDesktopChart();
} else {
	drawHeaderMobileChart();
}

if (viewportWidth > 993) {
	configureMenu();
}

///// LISTENERS /////
document.getElementById('arrowHeader').addEventListener('click', function(){
	document.getElementsByClassName('content')[0].scrollIntoView({behavior: "smooth"});
});

document.getElementById("menuToogle").addEventListener("click", function(e){
	e.preventDefault();
	if ( document.getElementById("menu").classList.contains("menu--toggled") ) {
		hideMenu();
	} else {
		showMenu();
	}
});

let menuLinks = document.getElementsByClassName("menu__link");
for (let i = 0; i < menuLinks.length; i++) {
	menuLinks[i].addEventListener("click", function(e){
		if ( !this.classList.contains("menu__link--external") ) {
			e.preventDefault();
			hideMenu();
			document.getElementById(this.getAttribute("href")).scrollIntoView({behavior: "smooth"});
		}
	});
}

window.addEventListener('scroll', function(){
	scrollActiv();

	let scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);

	//Visualización del ASIDE
	if ( scrollTop > viewportHeight ){
		sharing.classList.add("visible")
	} else {
		sharing.classList.remove("visible")
	}

	if (isMobile()) {
		if (isElementInViewport(footerAnchor)) {
			sharing.classList.remove("visible")
		}
	}

	for (var i = 0; i < sectionHeaders.length; i++) {
		if(sectionHeaders[i].getBoundingClientRect().top > 0 && sectionHeaders[i].getBoundingClientRect().top < 120) {
			if(window.scrollY > currentScroll){
				document.getElementById('menuTitle').textContent = menuTitles[i];
			} else {
				if(i == 0){
					document.getElementById('menuTitle').textContent = menuTitles[0];
				} else {
					document.getElementById('menuTitle').textContent = menuTitles[i-1];
				}				
			}
			currentScroll = window.scrollY;
		}
    }
});

/* MENU */
function configureMenu() {
	let menuItems = document.getElementsByClassName("menu__item");
	for (let i = 0; i < menuItems.length; i++ ) {
		menuItems[i].style.paddingLeft = `${document.getElementById("menuContainer").getBoundingClientRect().left}px`;
	}
}

function showMenu() {
	//Actualizamos icono
	document.getElementById("menuIcon").src = "https://www.ecestaticos.com/file/45a8e920a72504f5470249a7b517ba49/1614941274-icono-cerrar.svg";
	//Mostramos contenido del menú
	document.getElementById("menu").classList.add("menu--toggled");
	//Mostramos overlay sobre el contenido
	document.getElementById("overlay").classList.add("overlay--visible");
	//Deshabilitamos scroll en página
	document.body.style.overflow = "hidden"; 
}

function hideMenu() {
	//Actualizamos icono
	document.getElementById("menuIcon").src = "https://www.ecestaticos.com/file/5cc388f38ab4b808456b2fd0cb5a6e1f/1614936647-icono-menu.svg";
	//Ocultamos contenido del menú
	document.getElementById("menu").classList.remove("menu--toggled");
	//Ocultamos overlay sobre el contenido
	document.getElementById("overlay").classList.remove("overlay--visible");
	//Habilitamos scroll en página
	document.body.style.overflow = "auto"; 
}